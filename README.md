# vien-health-cc

Vien Health Code Challenge

## Client Code Challenge

You can find the client code challenge at <b>`/client-code-challenge`</b>.
It also contains a <b>`README.md`</b> file with sufficient instruction on how to run it.

## Nodejs Code Challenge

You can find the nodejs code challenge at <b>`/node-js-coding-challenge`</b>.
It also contains a <b>`README.md`</b> file with sufficient instruction on how to run it.

> NOTE: The code I added to the initial project can be found at <b>`/node-js-coding-challenge/api`</b>.
