import { FunctionComponent } from "react";
import Card, { CardProps } from "./Card";
import LineAreaChart, { LineAreaChartProps } from "./LineAreaChart";

export interface LineAreaCardProps {
  card: CardProps;
  chart: LineAreaChartProps;
}

const LineAreaCard: FunctionComponent<LineAreaCardProps> = ({
  card,
  chart,
}) => {
  return (
    <>
      <Card {...card} className="landscape">
        <LineAreaChart {...chart} />
      </Card>

      {/* STYLING */}
      <style jsx>{`
        :global(.landscape) {
          width: 55.4rem;
          max-width: 100%;
          height: 26.1rem;
        }

        /* :global(.recharts-wrapper) {
          width: 100% !important;
          height: 100% !important;
        }

        :global(.recharts-surface) {
          width: 100% !important;
          height: 100% !important;
        } */
      `}</style>
    </>
  );
};

export default LineAreaCard;
