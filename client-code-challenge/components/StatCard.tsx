import { FunctionComponent } from "react";
import BarChart from "./BarChart";
import Card, { CardProps } from "./Card";

export interface StatCardProps {
  card: CardProps;
  theme?: "primary" | "secondary";
  status: string;
  value: string;
  progress?: { percentage: number; label: string };
  barChartData?: { value: number; name: string }[];
}

const StatCard: FunctionComponent<StatCardProps> = ({
  card,
  theme,
  value,
  progress,
  barChartData,
  status,
}) => {
  return (
    <>
      <Card {...card} className="status">
        <div className="value-perc">
          <h3 className="value">{value}</h3>
          <p data-theme={theme} className="percentage">
            {status}
          </p>
        </div>

        {progress && (
          <>
            <div className="progress">
              <span
                data-theme={theme}
                style={{ width: `${progress.percentage}%` }}
              ></span>
            </div>
            <p className="label">{progress.label}</p>
          </>
        )}

        {barChartData && <BarChart data={barChartData} />}
      </Card>

      {/* STYLING */}
      <style jsx>{`
        :global(.status) {
          width: calc((100% / 3) - 5rem);
          /* height: 17.5rem; */
        }

        :global(.card__body) {
        }

        .value-perc {
          display: flex;
          justify-content: space-between;
          align-items: center;
        }

        .value {
          font-size: 3.6rem;
          line-height: 1;
          font-weight: normal;
          color: var(--color-heading);
        }

        .percentage[data-theme="primary"] {
          color: var(--color-primary);
        }

        .percentage[data-theme="secondary"] {
          color: #ff8433;
        }

        .progress {
          width: 100%;
          height: 0.4rem;
          margin: 1.6rem 0rem;
          background-color: #eaecee;
          border-radius: 2em;
          overflow: hidden;
        }

        .progress > span {
          display: block;
          height: 100%;
          background-color: var(--color-primary);
        }

        .progress > span[data-theme="primary"] {
          background-color: var(--color-primary);
        }

        .progress > span[data-theme="secondary"] {
          background-color: #ff8433;
        }
      `}</style>
    </>
  );
};

export default StatCard;
