import { FunctionComponent } from "react";

interface HambuggerButtonProps {
  onClick?(e: any): void;
}

const HambuggerButton: FunctionComponent<HambuggerButtonProps> = ({
  onClick,
}) => {
  return (
    <button className="hambugger-button" onClick={onClick}>
      <span></span>
      {/* STYLING */}
      <style jsx>{`
        button {
          width: 3.6rem;
          height: 3.6rem;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        span,
        span::before,
        span::after {
          display: block;
          height: 0.4rem;
          border-radius: 0.3rem;
          background-color: #1a1a1a;
        }

        span {
          position: relative;
          width: 2.4rem;
        }

        span::before,
        span::after {
          content: "";
          position: absolute;
          transition: width 0.3s ease-out;
        }

        span::before {
          top: -0.8rem;
          width: 1.8rem;
        }

        span::after {
          bottom: -0.8rem;
          width: 1.2rem;
        }

        button:hover span::before {
          width: 2.4rem;
        }

        button:hover span::after {
          width: 2.4rem;
        }
      `}</style>
    </button>
  );
};

export default HambuggerButton;
