import Container from "./Container";
import HambuggerButton from "./HambuggerButton";
import TagField from "./TagField";

const Navbar = () => {
  return (
    <nav className="navbar">
      <Container>
        <div className="search">
          <TagField
            placeholder="Search"
            rightAction={{
              icon: "/icons/search.svg",
              action: () => {},
              title: "Search",
            }}
            defaultTags={["Granger, IA"]}
          />
        </div>
        <div className="navbar__primary-links">
          <a href="#">Udwell now</a>
          <a href="#">Sell a property</a>
        </div>
        <HambuggerButton />
      </Container>

      {/* STYLING */}
      <style jsx>{`
        .navbar {
          width: 100%;
          height: 8rem;
          display: flex;
          position: relative;
          background-color: var(--color-white);
        }

        .navbar :global(.container) {
          height: 100%;
          display: flex;
          align-items: center;
        }

        .search {
          margin-right: auto;
        }

        .navbar__primary-links {
          margin-right: 7.3rem;
        }

        .navbar__primary-links a {
          color: #1a1a1a;
          margin-right: 4.3rem;
        }
      `}</style>
    </nav>
  );
};

export default Navbar;
