import { FunctionComponent, useState } from "react";
import Icon from "./Icon";

interface FieldAction {
  icon?: string;
  action(e: any): void | Promise<void>;
  title?: string;
}

interface Props {
  label?: string;
  placeholder?: string;
  name?: string;
  className?: string;
  rightAction?: FieldAction;
  type?: "text" | "url";
  defaultTags?: string[];
  onChange?(name: string, value: string[]): void | Promise<void>;
}

const TagField: FunctionComponent<Props> = ({
  label,
  name,
  placeholder,
  rightAction,
  type,
  className,
  defaultTags,
  onChange,
}) => {
  const [value, setValue] = useState("");
  const [tags, setTags] = useState(defaultTags || []);

  const createRemove = (key: number) => () => {
    setTags((prevTags) => prevTags.filter((_, i) => i !== key));
  };

  const reactToEnter = (e: any) => {
    // Detect `Enter` to add value to tags
    if (e.key === "Enter" && value) {
      setTags((prevTags) => [...prevTags, value]);
      setValue("");
      onChange && onChange(name, tags);
    }

    // Detect `Backspace` to remove a tag from tags
    // if only value is empty
    if (e.key === "Backspace" && value === "" && tags.length > 0) {
      setTags((prevTags) => {
        const update = [...prevTags];
        update.pop();
        return update;
      });
      onChange && onChange(name, tags);
    }
  };

  return (
    <div className={`tag-field ${className}`}>
      {label && <label htmlFor={name}>{label}</label>}

      <div className="tag-field__controls">
        <div className="tag-field__tags">
          {tags.map((tag, key) => (
            <div key={key} className="tag">
              <p>{tag}</p>
              <button onClick={createRemove(key)}>
                <Icon icon="/icons/cancel.svg" />
              </button>
            </div>
          ))}
        </div>

        <input
          onChange={(e) => setValue(e.target.value)}
          onKeyDown={reactToEnter}
          value={value}
          type={type}
          name={name}
          id={name}
          placeholder={tags.length > 0 ? "Add More" : placeholder}
        />
        {rightAction && (
          <button
            className="right-action"
            onClick={rightAction.action}
            title={rightAction.title}
          >
            <img src={rightAction.icon} alt={rightAction.title} />
          </button>
        )}
      </div>

      {/* STYLING */}
      <style jsx>{`
        .tag-field__controls {
          width: 34.6rem;
          height: 5rem;
          border-radius: 0.6rem;
          background-color: #f5f5f5;
          display: flex;
          align-items: center;
          overflow: hidden;
          position: relative;
          padding-left: 1.5rem;
        }

        .tag-field label {
          color: var(--color-body-text2);
        }

        .tag-field__controls input {
          flex-grow: 1;
          flex-shrink: 1;
          padding-left: 1.5rem;
          background-color: inherit;
          width: 100%;
        }

        .tag-field__controls input:focus {
          outline: none;
        }

        .tag-field__controls .right-action {
          flex-grow: 0;
          flex-shrink: 0;
          width: 5.4rem;
          height: 100%;
          border-left: 0.1rem solid #d1d1d1;
          background-color: var(--color-primary);
        }

        .tag-field__tags {
          flex-shrink: 0;
          display: flex;
          gap: 0.5rem;
          max-width: 14rem;
        }

        .tag {
          border-radius: 0.6rem;
          padding: 0.2rem 0.8rem;
          background-color: #e8e8e8;
          display: flex;
          height: 3rem;
        }

        .tag p {
          margin-right: 0.6rem;
        }
      `}</style>
    </div>
  );
};

export default TagField;
