import { FunctionComponent } from "react";

interface Props {
  design?: "h2";
}

const Heading: FunctionComponent<Props> = ({ design, children }) => {
  return (
    <div data-design={design} className="heading">
      {children}

      {/* STYLING */}
      <style jsx>{`
        [data-design="h2"] {
          font-size: 1.8rem;
          line-height: 1.6;
          color: var(--color-heading);
          font-weight: bold;
        }
      `}</style>
    </div>
  );
};

export default Heading;
