import { FunctionComponent } from "react";

interface FieldAction {
  icon?: string;
  action(e: any): void | Promise<void>;
  title?: string;
}

interface Props {
  label?: string;
  placeholder?: string;
  name?: string;
  className?: string;
  rightAction?: FieldAction;
  type?: "text" | "url";
  onChange?(e: any): void | Promise<void>;
}

const TextField: FunctionComponent<Props> = ({
  label,
  name,
  placeholder,
  rightAction,
  type,
  className,
  onChange,
}) => {
  return (
    <div className={`text-field ${className}`}>
      {label && <label htmlFor={name}>{label}</label>}
      <div className="text-field__controls">
        <input
          onChange={onChange}
          type={type}
          name={name}
          id={name}
          placeholder={placeholder}
        />
        {rightAction && (
          <button onClick={rightAction.action} title={rightAction.title}>
            <img src={rightAction.icon} alt={rightAction.title} />
          </button>
        )}
      </div>

      {/* STYLING */}
      <style jsx>{`
        .text-field__controls {
          width: 24.8rem;
          height: 5rem;
          border-radius: 0.6rem;
          border: 0.1rem solid #d1d1d1;
          display: flex;
          overflow: hidden;
        }

        .text-field label {
          color: var(--color-body-text2);
        }

        .text-field__controls input {
          flex-grow: 1;
          padding-left: 1.5rem;
        }

        .text-field__controls input:focus {
          outline: none;
          background-color: #e5e5e5;
        }

        .text-field__controls button {
          flex-grow: 0;
          flex-shrink: 0;
          width: 5.4rem;
          border-left: 0.1rem solid #d1d1d1;
        }
      `}</style>
    </div>
  );
};

export default TextField;
