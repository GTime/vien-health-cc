import Image from "next/image";
import Container from "./Container";
import Heading from "./Heading";

const Footer = () => {
  return (
    <div className="footer">
      <Container>
        <div className="footer__columns">
          <div className="location">
            <p>Counter Delivery, Carters Beach PostCentre, Westport</p>
          </div>
          <div className="link-group">
            <Heading design="h2">About</Heading>
            <a href="#">Company</a>
            <a href="#">Team</a>
            <a href="#">Careers</a>
            <a href="#">Blog</a>
          </div>
          <div className="link-group">
            <Heading design="h2">Support</Heading>
            <a href="#">Help Center</a>
            <a href="#">Cancellation Options</a>
            <a href="#">Neigborhood Support</a>
            <a href="#">Trust & Safety</a>
          </div>
          <div className="link-group address">
            <Heading design="h2">Address</Heading>
            <p>Counter Delivery, Carters Beach PostCentre, Westport</p>
            <div className="address__social-media">
              <a href="#">
                <Image
                  src="/icons/facebook.svg"
                  width={20}
                  height={20}
                  alt=""
                />
              </a>
              <a href="#" data-state="active">
                <Image
                  src="/icons/instagram.svg"
                  width={20}
                  height={20}
                  alt=""
                />
              </a>
              <a href="#">
                <Image src="/icons/twitter.svg" width={20} height={20} alt="" />
              </a>
            </div>
          </div>
        </div>
        <div className="footer__copyright">
          <a href="#">©Udwell, LLC. All rights reserved.</a>
          <a href="#">Terms & Conditions</a>
          <a href="#">Privacy Policy</a>
        </div>
      </Container>

      {/* STYLING */}
      <style jsx>{`
        .footer {
          width: 100%;
          display: flex;
          position: relative;
          margin-top: auto;
          padding: 2.5rem 0rem;
        }

        .footer :global(.container) {
          display: flex;
          flex-direction: column;
        }

        .footer__columns {
          display: flex;
          width: 100%;
          margin-bottom: 4rem;
        }

        .footer__columns > * {
          width: calc(100% / 4);
          min-height: 19rem;
          padding-right: 8.1rem
        }

        .location {
          display: flex;
          align-items: center;
        }

        .location p {
          max-width: 207px;
        }

        .link-group :global(.heading) {
          margin-bottom: 1.6rem;
        }
        
        .link-group a {
          display: block;
          margin-bottom: 0.8rem;
        }


        /* Address */
        .address p {
          margin-bottom: 3.6rem;
        }
        

        .address__social-media  {
          display: flex;
        }

        .address__social-media a  {
          width: 5rem;
          height: 5rem;
          border-radius: 50%;
          background-color: var(--color-white);
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .address__social-media a:not(:last-child) {
          margin-right: 2.5rem;
        }

        .address__social-media a[data-state="active"]{
          background-color: var(--color-primary);
        }

        .footer__copyright {
          margin: 0 auto;
        }

        .footer__copyright:not(:first-child)::before {
          content: "";
          background-color: #a3a3a3;
          width: 0.3rem;
          height: 0.3rem;
          border-radius: : 50%;
        }
      `}</style>
    </div>
  );
};

export default Footer;
