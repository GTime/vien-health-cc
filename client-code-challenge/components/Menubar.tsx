import Heading from "./Heading";
import Icon from "./Icon";
import TextField from "./TextField";

const menuItems = [
  { title: "Profile", link: "#profile", icon: "/icons/user.svg" },
  { title: "My listing", link: "#", icon: "/icons/listing.svg" },
  { title: "Saved homes", link: "#", icon: "/icons/saved-homes.svg" },
  { title: "Saved search", link: "#", icon: "/icons/saved-search.svg" },
  { title: "Messages", link: "#", icon: "/icons/messages.svg" },
  { title: "Notifications", link: "#", icon: "/icons/notifications.svg" },
  { title: "Billings", link: "#", icon: "/icons/billing.svg" },
  { title: "Analytics", link: "#", icon: "/icons/analytics.svg" },
  { title: "Blog", link: "#", icon: "/icons/blog.svg" },
];

const Menubar = () => {
  const activeLink = "#profile";

  return (
    <nav className="menubar">
      <div className="menubar__profile">
        <div className="avatar">
          <img src="/images/profile.png" alt="Profile Image" />
        </div>
        <Heading design="h2">Alex Assenmacher</Heading>
        <p>Home Buyer</p>
        <button>Edit Profile</button>
      </div>

      <div className="menubar__items">
        {menuItems.map((item, key) => (
          <a data-active={activeLink === item.link} href={item.link} key={key}>
            <span>{item.title}</span>
            <Icon icon={item.icon} />
          </a>
        ))}
      </div>

      <TextField
        type="url"
        name="profileLinkInput"
        label="Profile link"
        placeholder="https://www.yourlink.com"
        className="menubar__profile-link"
        rightAction={{
          title: "Copy profile link",
          action: () => {},
          icon: "/icons/copy.svg",
        }}
      />

      {/* STYLING */}
      <style jsx>{`
        .menubar {
          width: 28.8rem;
          max-width: 100%;
          min-height: 81.4rem;
          display: flex;
          flex-direction: column;
          position: relative;
          border-radius: 0.6rem;
          background-color: var(--color-white);
          padding: 2.4rem 2rem;
          margin-right: 2.7rem;
        }

        .menubar__profile {
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
          padding-bottom: 2.4rem;
        }

        .menubar__profile .avatar {
          width: 7.8rem;
          height: 7.8rem;
          border-radius: 50%;
          background-color: #e8e8e8;
          margin-bottom: 1.2rem;
        }

        .menubar__profile .avatar img {
          width: 100%;
          height: 100%;
          object-fit: cover;
          object-position: center;
        }

        .menubar__profile p {
          margin-bottom: 1.2rem;
        }

        .menubar__profile button {
          background-color: var(--color-primary);
          color: var(--color-white);
          padding: 0.7rem 3.2rem;
          border-radius: 0.6rem;
        }

        .menubar__items {
          padding: 2rem 0rem;
          flex-grow: 1;
        }

        .menubar__items a {
          border-top: 0.1rem solid #e8e8e8;
          display: flex;
          justify-content: space-between;
          align-items: center;
          padding: 1.2rem;
          color: var(--color-body-text2);
        }

        .menubar__items a:last-child {
          border-bottom: 0.1rem solid #e8e8e8;
        }

        .menubar__items a:hover,
        .menubar__items a[data-active="true"] {
          font-weight: bold;
        }
      `}</style>
    </nav>
  );
};

export default Menubar;
