import { FunctionComponent } from "react";
import Icon from "./Icon";
import { CardTheme } from "./Card";

export interface CardHeaderProps {
  className?: string;
  icon?: string;
  title?: string;
  subtile?: string;
  status?: string;
  menu?: any;
  bordered?: boolean;
  theme?: CardTheme;
}

const CardHeader: FunctionComponent<CardHeaderProps> = ({
  className,
  icon,
  title,
  subtile,
  status,
  menu,
  bordered,
  theme,
}) => {
  return (
    <div
      data-bordered={bordered}
      data-theme={theme}
      className={`card-header ${className}`}
    >
      {icon && (
        <div className="icon">
          <Icon icon={icon} />
        </div>
      )}

      <h2>
        {title && <span className="title">{title}</span>}
        {status && <span className="status">{status}</span>}
      </h2>

      {menu && (
        <button>
          <span></span>
        </button>
      )}

      {subtile && <p>{subtile}</p>}

      {/* STYLING */}
      <style jsx>{`
        .card-header {
          width: 100%;
          height: 6.8rem;
          display: flex;
          align-items: center;
          padding: 1rem 2rem;
        }

        [data-bordered="true"] {
          border-bottom: 0.1rem solid #eaedf3;
        }

        .icon {
          width: 3.6rem;
          height: 3.6rem;
          display: flex;
          justify-content: center;
          align-items: center;
          border-radius: 0.32rem;
          background-color: var(--color-primary);
          margin-right: 1.7rem;
        }

        [data-theme="secondary"] .icon {
          background-color: var(--color-secondary);
        }

        h2 {
          flex-grow: 1;
        }

        h2 .title {
          font-size: 1.6rem;
          color: var(--color-heading);
        }

        h2 .status {
          display: block;
          width: 100%;
          color: #34aa44;
          font-size: 1.2rem;
          line-height: 1;
          font-weight: normal;
        }

        [data-theme="secondary"] .status {
          color: var(--color-outline);
        }

        button {
          width: 3.6rem;
          height: 3.6rem;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        button span,
        button span::before,
        button span::after {
          display: block;
          width: 0.4rem;
          height: 0.4rem;
          border-radius: 50%;
          background-color: #9ea0a5;
        }

        button span {
          position: relative;
        }

        button span::before,
        button span::after {
          content: "";
          position: absolute;
        }

        button span::before {
          left: -0.7rem;
        }

        button span::after {
          right: -0.7rem;
        }
      `}</style>
    </div>
  );
};

export default CardHeader;
