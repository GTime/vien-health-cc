import { FunctionComponent } from "react";
import Card, { CardProps } from "./Card";
import PieChart, { PieChartProps } from "./PieChart";

export interface PieCardProps {
  card: CardProps;
  pieChart: PieChartProps;
  stats: { label: string; value: string }[];
}

const PieCard: FunctionComponent<PieCardProps> = ({
  card,
  pieChart,
  stats,
}) => {
  return (
    <>
      <Card className="pie-card" {...card}>
        <div className="frame">
          <div className="chart">
            <PieChart {...pieChart} />
          </div>
          {stats.map(({ label, value }, key) => (
            <div className="item" key={key}>
              <p className="item_value">{value}</p>
              <p className="item_label">{label}</p>
            </div>
          ))}
        </div>
      </Card>

      {/* STYLING */}
      <style jsx>{`
        :global(.pie-card) {
          width: 28rem;
          max-width: 100%;
          height: 26.1rem;
        }

        .frame {
          width: 100%;
          position: relative;
          display: grid;
          grid-template-columns: auto 10.2rem;
          grid-template-rows: 50% 50%;
          height: 100%;
        }

        .chart {
          grid-column: 1/2;
          grid-row: 1/-1;
          position: relative;
          overflow: hidden;
        }

        .item:not(:last-child) {
          border-bottom: 0.1rem solid #eaecee;
        }

        .item {
          text-align: right;
          line-height: 1.38;
          padding: 1.5rem 0.4rem;
        }

        .item_value {
          font-size: 2rem;
          color: var(--color-body-text2);
        }

        .item_label {
          font-size: 1.1rem;
          color: var(--color-outline);
        }

        :global(.recharts-wrapper) {
          width: 100% !important;
          height: 100% !important;
        }

        :global(.recharts-surface) {
          width: 100% !important;
          height: 100% !important;
        }
      `}</style>
    </>
  );
};

export default PieCard;
