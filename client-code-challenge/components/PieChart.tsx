import { FunctionComponent } from "react";
import { PieChart, Pie, Cell } from "recharts";

export interface PieChartProps {
  data: {
    name: string;
    value: number;
    color: string;
    prefix?: string;
  }[];
}

const CustomPieChart: FunctionComponent<PieChartProps> = ({ data }) => {
  return (
    <PieChart width={164} height={100}>
      <Pie
        data={data}
        cx={78}
        cy={45}
        innerRadius={50}
        outerRadius={80}
        paddingAngle={0}
        dataKey="value"
      >
        {data.map(({ color }, index) => (
          <Cell key={`cell-${index}`} fill={color} />
        ))}
      </Pie>
    </PieChart>
  );
};

export default CustomPieChart;
