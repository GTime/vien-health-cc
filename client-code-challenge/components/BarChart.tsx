import React, { FunctionComponent } from "react";
import {
  BarChart as RechartBarChart,
  Bar,
  Cell,
  ResponsiveContainer,
} from "recharts";

export interface BarChartProps {
  data: { name: string; value: number }[];
}

const BarChart: FunctionComponent<BarChartProps> = ({ data }) => {
  return (
    <ResponsiveContainer width="100%" height={61}>
      <RechartBarChart width={150} height={40} data={data}>
        <Bar
          dataKey="value"
          // onClick={'this.handleClick'}
        >
          {data.map((entry, index) => (
            <Cell
              cursor="pointer"
              // fill={index === activeIndex ? "#82ca9d" : "#8884d8"}
              fill={"#FF8433"}
              key={`cell-${index}`}
            />
          ))}
        </Bar>
      </RechartBarChart>
    </ResponsiveContainer>
  );
};

export default BarChart;
