import React, { FunctionComponent } from "react";
import {
  ComposedChart,
  Line,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";

export interface LineAreaChartProps {
  data: { name: string; value: number }[];
  color: { line: string; area: string };
}

const LineAreaChart: FunctionComponent<LineAreaChartProps> = ({
  data,
  color,
}) => {
  return (
    <ResponsiveContainer width="108%" height={175} >
      <ComposedChart
        // width={560}
        // height={190}
        data={data}
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
      >
        <CartesianGrid stroke="#eaedf3" vertical={false} />
        <XAxis
          dataKey="name"
          stroke="#ff843338"
          tick={{ strokeWidth: 0.1, fill: "#9EA0A5" }}
        />
        <YAxis
          orientation="right"
          stroke="#00000000"
          unit="k"
          tick={{ strokeWidth: 0.1, fill: "#9EA0A5" }}
        />
        <Tooltip />
        {/* <Legend /> */}
        <Area type="normal" dataKey="value" fill={color.area} />
        <Line
          type="normal"
          dataKey="value"
          stroke={color.line}
          activeDot={true}
          strokeWidth={1.52}
          dot={{ strokeWidth: 0, fill: color.line }}
        />
      </ComposedChart>
    </ResponsiveContainer>
  );
};

export default LineAreaChart;
