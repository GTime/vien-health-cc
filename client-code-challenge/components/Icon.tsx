import { FunctionComponent } from "react";

interface Props {
  icon: string;
}

const Icon: FunctionComponent<Props> = ({ icon }) => {
  return (
    <>
      <img src={icon} alt="Icon" />

      {/* STYLING */}
      <style jsx>{`
        .icon {
          width: 100%;
          height: 100%;
          object-fit: cover;
          object-position: center;
        }
      `}</style>
    </>
  );
};

export default Icon;
