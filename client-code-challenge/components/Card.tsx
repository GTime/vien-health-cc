import { FunctionComponent } from "react";
import CardHeader, { CardHeaderProps } from "./CardHeader";

export type CardTheme = "primary" | "secondary";

export interface CardProps {
  className?: string;
  header?: CardHeaderProps;
}

const Card: FunctionComponent<CardProps> = ({
  className,
  header,
  children,
}) => {
  return (
    <div className={`card ${className}`}>
      {header && <CardHeader {...header} />}
      <div className="card__body">{children}</div>

      {/* STYLING */}
      <style jsx>{`
        .card {
          min-width: 26.9rem;
          min-height: 17.5rem;
          border-radius: 0.4rem;
          border: 0.1rem solid #eaedf3;
          box-shadow: 0rem 0.1rem 0.3rem rgba(0, 0, 0, 0.04);
          display: grid;
          grid-template-columns: 1fr;
        }

        .card__body {
          padding: 2rem;
        }
      `}</style>
    </div>
  );
};

export default Card;
