const Container = ({ children }) => {
  return (
    <div className="container">
      {children}

      {/* STYLING */}
      <style jsx>{`
        .container {
          width: 123.4rem;
          max-width: 100%;
          margin: auto auto;
        }
      `}</style>
    </div>
  );
};

export default Container;
