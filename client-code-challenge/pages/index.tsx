import Head from "next/head";
import Card from "../components/Card";
import Container from "../components/Container";
import Menubar from "../components/Menubar";
import StatCard from "../components/StatCard";
import PieCard, { PieCardProps } from "../components/PieCard";
import LineAreaCard, { LineAreaCardProps } from "../components/LineAreaCard";
// import Image from 'next/image'

const totalSales: PieCardProps = {
  card: {
    header: {
      title: "Total Sales",
      icon: "/icons/money.svg",
      status: "+$985.56",
      menu: "#",
      theme: "primary",
      bordered: true,
    },
  },

  pieChart: {
    data: [
      { name: "Not Invoiced", value: 200, color: "#EAECEE" },
      { name: "Invoiced", value: 800, color: "#016450", prefix: "$" },
    ],
  },

  stats: [
    { label: "Invoiced", value: "$2,595" },
    { label: "Invoices", value: "23" },
  ],
};

const userOnboarding: PieCardProps = {
  card: {
    header: {
      title: "User Onboarding",
      icon: "/icons/profile-round.svg",
      status: "Q3 Goal: 8,000 User",
      menu: "#",
      theme: "secondary",
      bordered: true,
    },
  },

  pieChart: {
    data: [
      { name: "Not Invoiced", value: 200, color: "#FACF55" },
      { name: "Onboarded", value: 800, color: "#EAECEE" },
    ],
  },

  stats: [
    { label: "Unboarded", value: "2,452" },
    { label: "   ", value: "" },
  ],
};

const data = [
  {
    name: "Mon",
    value: 8.3,
  },
  {
    name: "Tue",
    value: 5.2,
  },
  {
    name: "Wed",
    value: 6,
  },
  {
    name: "Thu",
    value: 9,
  },
  {
    name: "Fri",
    value: 8,
  },
  {
    name: "Sat",
    value: 4,
  },
  {
    name: "Sun",
    value: 5.5,
  },

  {
    name: "",
    value: 5.5,
  },
];

const dailyProfit: LineAreaCardProps = {
  card: {
    header: { title: "Daily profit", menu: "#", bordered: true },
  },
  chart: { data, color: { line: "#FF8433", area: "#ff843338" } },
};

const clients: LineAreaCardProps = {
  card: {
    header: {
      title: "Daily active clients",
      menu: "#",
      bordered: true,
    },
  },
  chart: { data, color: { line: "#016450", area: "#01645038" } },
};

export default function Home() {
  return (
    <div className="home">
      <Head>
        <title>Code Challenge</title>
        <meta name="description" content="Code challenge from Vien Health" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container>
        <Menubar />
        <main>
          <StatCard
            card={{
              header: { title: "Sales", subtile: "Monthly Goal" },
            }}
            theme="primary"
            status="+6.9%"
            value="98.1%"
            progress={{ percentage: 80, label: "Yearly Goal" }}
          />

          <StatCard
            card={{
              header: { title: "Revenue", subtile: "Total Profit" },
            }}
            theme="secondary"
            status="+11.3%"
            value="$13,893"
            barChartData={data}
          />

          <StatCard
            card={{
              header: { title: "Clients", subtile: "Subscribed" },
            }}
            theme="primary"
            status="+3.2%"
            value="$1232"
            progress={{ percentage: 80, label: "Yearly Goal" }}
          />

          <PieCard {...totalSales} />
          <LineAreaCard {...dailyProfit} />
          <LineAreaCard {...clients} />
          <PieCard {...userOnboarding} />
        </main>
      </Container>

      {/* STYLING */}
      <style jsx>{`
        .home {
          display: flex;
          margin: 0 auto;
          padding: 3rem;
        }

        .home :global(.container) {
          min-height: 81.4rem;
          display: flex;
        }

        main {
          flex-grow: 1;
          /* width: 28.8rem; */
          max-width: 100%;
          min-height: 81.4rem;
          position: relative;
          border-radius: 0.6rem;
          background-color: var(--color-white);
          padding: 3rem;
          display: flex;
          gap: 2.5rem;
          flex-wrap: wrap;
          align-items: flex-start;
        }
      `}</style>
    </div>
  );
}
