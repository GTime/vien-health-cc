import Head from "next/head";
import Card from "../components/Card";
import Container from "../components/Container";
import LineAreaChart from "../components/LineAreaChart";
import PieChart from "../components/PieChart";
// import Image from 'next/image'

export default function Home() {
  return (
    <div className="home">
      <Head>
        <title>Code Challenge</title>
        <meta name="description" content="Code challenge from Vien Health" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container>
        <LineAreaChart />
        <PieChart />
      </Container>

      {/* STYLING */}
      <style jsx>{`
        .home {
          display: flex;
          margin: 0 auto;
          padding: 3rem;
        }

        .home :global(.container) {
          min-height: 81.4rem;
          display: flex;
        }

        main {
          flex-grow: 1;
          /* width: 28.8rem; */
          max-width: 100%;
          min-height: 81.4rem;
          position: relative;
          border-radius: 0.6rem;
          background-color: var(--color-white);
          padding: 3rem;
        }

        .status {
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(26.9rem, 1fr));
          gap: 2.5rem;
        }
      `}</style>
    </div>
  );
}
