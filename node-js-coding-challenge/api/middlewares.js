const account = require("./services/account");
const jwt = require("jsonwebtoken");
const { ACCESSTOKEN_SECRET } = require("./config");

function setContext(database) {
  return async (req, _res, next) => {
    const authorization = req.headers.authorization;
    const context = { database, user: null };

    // Retrieve access token
    if (req.headers.authorization) {
      const token = authorization.split(" ")[1];
      const decoded = jwt.decode(token);

      // Verify access token
      if (decoded) {
        try {
          jwt.verify(token, ACCESSTOKEN_SECRET + decoded.sub);

          // Find and set user in context
          const result = await new account.Repo(database).findById(decoded.sub);
          if (result.isOk()) context.user = result.value;
        } catch (error) {
          console.log("Access by unverified! ");
        }
      }
    }

    req["context"] = context; // Attach context to request
    next();
  };
}

module.exports = {
  setContext,
};
