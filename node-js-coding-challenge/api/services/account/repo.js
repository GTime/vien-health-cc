const { Result, CustomError } = require("../../utils/result");
const User = require("./user");

class UserRepo {
  constructor(database) {
    this.collection = database.collection("user");
  }

  async findById(id) {
    const user = await this.collection.findOne({ _id: id });

    return user
      ? Result.ok(new User(Mapper.toDomain(user)))
      : Result.err({ message: "User not found" });
  }

  async findByEmail(email) {
    const user = await this.collection.findOne({ email });

    return user
      ? Result.ok(new User(Mapper.toDomain(user)))
      : Result.err({ message: "User not found" });
  }

  async doesUserExist(email) {
    return (await this.collection.findOne({ email })) ? true : false;
  }

  async save(user) {
    const savedUser = await this.collection.insertOne(
      Mapper.toPersistence(user)
    );

    return savedUser.acknowledged
      ? Result.ok(new User(user))
      : CustomError.technical("Failed to save user").toResult();
  }
}

class Mapper {
  static toDomain(rawUser) {
    return {
      id: rawUser._id,
      name: rawUser.name,
      email: rawUser.email,
      password: rawUser.password,
    };
  }

  static toPersistence(user) {
    return {
      _id: user.id,
      name: user.name,
      email: user.email,
      password: user.password,
    };
  }
}

module.exports = UserRepo;
