const express = require("express");
const { CustomError } = require("../../utils/result");
const registerUser = require("./usecases/register");
const getProfile = require("./usecases/getProfile");
const loginUser = require("./usecases/loginUser");

const router = express.Router();
const technicalError = {
  ...CustomError.technical("Encountered a technical issue please try again"),
};

// Register
router.post("/register", async (req, res) => {
  try {
    const result = await registerUser(req.context, req.body);

    // Check for success
    if (!result.isErr()) {
      res.status(201).json(result.value);
    } else {
      res.status(400).json(result.value);
    }
  } catch (error) {
    res.status(500).json(technicalError);
  }
});

// Login
router.post("/login", async (req, res) => {
  try {
    const result = await loginUser(req.context, req.body);

    // Check for success
    if (!result.isErr()) {
      res.status(200).json(result.value);
    } else {
      res.status(400).json(result.value);
    }
  } catch (error) {
    // console.log(technicalError, error);
    res.status(500).json(technicalError);
  }
});

// Profile
router.get("/profile", async (req, res) => {
  try {
    const result = await getProfile(req.context, req.body);

    // Check for success
    if (!result.isErr()) {
      res.status(200).json(result.value);
    } else {
      res.status(401).json(result.value);
    }
  } catch (error) {
    res.status(500).json(technicalError);
  }
});

// Logout
router.post("/logout", function (req, res) {
  res.status(200).json({ token: "0x000" });
});

module.exports = router;
