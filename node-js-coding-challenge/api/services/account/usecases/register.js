const { Result, CustomError } = require("../../../utils/result");
const UserRepo = require("../repo");
const User = require("../user");

async function registerUser(context, unvalidatedUser) {
  // Validity unvalidatedUser
  const { valid, message, errors } = User.validateForRegister(unvalidatedUser);

  // Report validity
  if (!valid) return CustomError.userInput(message, errors).toResult();

  const repo = new UserRepo(context.database);

  // Check if user already exist
  if (await repo.doesUserExist(unvalidatedUser.email)) {
    return Result.err("You are aready registered, please try logging in");
  }

  const user = await User.create(unvalidatedUser); // Create new user and attach id
  const result = await repo.save(user.toBeSaved()); // Persist user
  const token = user.generateAccessToken(); // Generate access token

  return result.isErr() ? result : Result.ok({ token });
}

module.exports = registerUser;
