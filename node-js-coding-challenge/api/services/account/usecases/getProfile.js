const { CustomError } = require("../../../utils/result");
const UserRepo = require("../repo");

async function getProfile(context) {
  // Report validity
  if (!context.user)
    return CustomError.authentication("Please logging in").toResult();

  // Find user
  return await new UserRepo(context.database).findById(context.user.id);
}

module.exports = getProfile;
